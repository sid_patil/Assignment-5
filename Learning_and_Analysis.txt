Results:
Accuracy using 2 layer Neural Network: 92.8983%

Accuracy using Convolutional Neural Networks with 3 convolutional layers: 92.8466%

Accuracy using Convolutional Neural Networks with 3 convolutional layers and Data Augmentation: 92.2916%

Accuracy using Convolutional Neural Networks with 3 convolutional layers and Batch Normalization: 94.0550%

Accuracy using Convolutional Neural Networks with 3 convolutional layers, Batch Normalization and Data Augmentation: 93.0883%

As can be seen the Accuracy decreases after Data Augmentation. I think this is happening because the training and testing data given to us is clean and able to define a model well using the size of data given to us. I would be grateful to get more insights on this problem. 

